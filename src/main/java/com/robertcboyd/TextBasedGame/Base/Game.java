package com.robertcboyd.TextBasedGame.Base;

import java.io.File;

import com.robertcboyd.TextBasedGame.Parser.WordParser;

/**
 * The Game class sets up the game and starts it, but
 * should not be used to hold anything more than basic
 * initialization. It is preferred that this class be
 * left relatively clean.
 */
public class Game {

	/**
	 * The main method.
	 *
	 * @param args Not used.
	 */
	public static void main(String[] args)
	{
		//game start
		UI ui = new UI(new java.util.Scanner(System.in));
		PlayerFactory playerFactory = new PlayerFactory(ui);
		WordParser wp = new WordParser();
		WorldManager wm = new WorldManager();

		PlaceFactory placeFactory = new PlaceFactory(wp, ui, wm);
		World places = new World(placeFactory);

		Player player = playerFactory.createPlayer(places.firstLocation);

		while(!gameOver()) {
			player.getLocation().onEnter(player);
		}
	}

	/**
	 * Decides if the game is over.
	 *
	 * @return True if the game is over, false otherwise.
	 */
	public static boolean gameOver() {
		return false;
		//TODO for now
	}

	private static Logger logger;
	public static Logger getLogger(){
		if(logger == null){
			//init logger
			//be sure that the logfile is unique
			int i = 0;
			while(new File("log-" + i + ".txt").isFile()){
				i++;
			}
			logger = new Logger("log-" + i + ".txt");
		}
		return logger;
	}
}
