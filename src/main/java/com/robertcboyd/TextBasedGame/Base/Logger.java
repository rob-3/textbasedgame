package com.robertcboyd.TextBasedGame.Base;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class Logger {
	private File logFile;

	public void log(String s)
	{
		try{
			FileUtils.writeStringToFile(logFile, s + "\n", "UTF-8", true);
		} catch (IOException e){
			//TODO do something about it
		}
	}

	public Logger(String filename)
	{
		this.logFile = FileUtils.getFile(filename);
	}
}
