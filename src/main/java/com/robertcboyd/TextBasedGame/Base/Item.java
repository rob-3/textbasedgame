package com.robertcboyd.TextBasedGame.Base;

import java.util.ArrayList;

import com.robertcboyd.TextBasedGame.GameObjectInterfaces.GameObject;
import com.robertcboyd.TextBasedGame.Parser.Word;

public abstract class Item extends GameObject {

	//TODO create ItemFactory
	public Item(UI ui, String name, String description, Place location, boolean visible, ArrayList<String> identifiers)
	{
		super(ui, name, description, location, visible, identifiers);
	}
}
