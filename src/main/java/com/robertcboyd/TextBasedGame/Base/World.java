package com.robertcboyd.TextBasedGame.Base;

import com.robertcboyd.TextBasedGame.Parser.WordParser;

/**
 * Holding class for all Places.
 * The setup() method should contain any code necessary to
 * set up the Places.
 */
public class World {

	public final Place firstLocation;

	public final Place hill; 
	public final Place grasslands;
	public final Place abandonedCastleFront;
	public final Place abandonedCastleCornerSE;
	public final Place abandonedCastleCornerSW;
	public final Place swarshySwamp;
	public final Place graniteObelisk;

	/**
	 * Called before the game starts. Place any initialization code
	 * for your Places here. The most common use case is connecting
	 * Places automatically with the proper Exits.
	 */
	private void setup()
	{
		//TODO offload this to another thread

		grasslands.attachToNorthOf(hill);
		swarshySwamp.attachToEastOf(hill);
		abandonedCastleFront.attachToNorthOf(grasslands);
		abandonedCastleCornerSE.attachToEastOf(abandonedCastleFront);
		abandonedCastleCornerSW.attachToWestOf(abandonedCastleFront);
		graniteObelisk.attachToNorthOf(swarshySwamp);
	}

	/**
	 * Places is a holding object for every Place in the game.
	 * It handles initialization and loading for these objects.
	 * Lazy loading can also be ultimately implemented here.
	 *
	 * @param pf a PlaceFactory to create Places
	 */
	public World(PlaceFactory pf)
	{
		this.hill = pf.createPlace("Hill", "You walk up a long path to the top of a nearby hill. The air is clear and there is a castle far to the north.");
		this.grasslands = pf.createPlace("You reach a grasslands area at the foot of a hill to the south. To the north there is a castle.");

		this.abandonedCastleFront = pf.createPlace("Castle Front", "You are at the front of what appears to be a ruined castle. The castle has 4 major towers; the nearest ones - those to the southeast and the southwest - are in tatters. The northern towers still appear relatively intact.");
		this.abandonedCastleCornerSE = pf.createPlace("Castle SE tower", "A tower (in shambles) rises upon this corner of the castle.");
		this.abandonedCastleCornerSW = pf.createPlace("Castle SW tower", "A ruined tower rests above. Moss covers the stones still standing. A few large stones are scattered around the area.");
		this.swarshySwamp = pf.createPlace("Swarshy Swamp", "Muck and vines lather the faces of the trees.");
		this.graniteObelisk = pf.createPlace("Obelisk of the Lathenheim Fiends", "A shiny black and red tower rises into the swirling purple sky.");
		
		this.setup();

		//set firstLocation
		this.firstLocation = hill;
	}
}
