package com.robertcboyd.TextBasedGame.Base;

import java.util.Scanner;

import org.apache.commons.text.WordUtils;

/**
 * Utility class with methods to get and print to the console.
 * Largely wraps System.out and System.in
 */
public class UI {

	private Scanner scanner;

	/**
	 * Asks the user for input in a nice way with a colon for a prompt.
	 *
	 * @return A String with the user's input.
	 */
	public String getInput()
	{
		println();
		println("What would you like to do?");
		print(":");
		String tmp = scanner.nextLine();
		println();
		return tmp;
		//TODO catch scanner errors
	}

	/**
	 * Wraps System.out.print(). Should be used any time one wishes to print information to the user console in a manner like System.out.print().
	 *
	 * @param string The String to be printed.
	 */
	public void print(String string)
	{
		System.out.print(WordUtils.wrap(string, 80));
	}

	/**
	 * Wraps System.out.println(). Should be used any time one wishes to print information to the user console in a manner like System.out.println().
	 *
	 * @param string The String to be printed.
	 */
	public void println(String string)
	{
		System.out.println(WordUtils.wrap(string, 80));
	}

	/**
	 * Prints a blank line. Simply a shorthand for UI.println("").
	 */
	public void println()
	{
		System.out.println("");
	}

	/**
	 * Prints the stringToPrint in a box. Will print nothing if the String
	 * is null or empty. A newline is printed after the box.
	 *
	 * @param stringToPrint the String to be printed
	 * @param heavy whether a "heavy" or bold box will be used
	 */
	public void printInBox(String stringToPrint, boolean heavy)
	{
		if(stringToPrint == null || stringToPrint.equals("")){
			return;
		}

		if(heavy){
			//top line of box; index 1 to allow the different first character
			this.print("┏");
			for(int i = 1; i <= stringToPrint.length(); i++){
				this.print("━");
			}
			this.println("┓");

			//middle line of box
			this.println("┃" + stringToPrint + "┃");

			//bottom line of box; index 1 to allow the different first character
			this.print("┗");
			for(int i = 1; i <= stringToPrint.length(); i++){
				this.print("━");
			}
			this.println("┛");
		} else {
			//top line of box; index 1 to allow the different first character
			this.print("┌");
			for(int i = 1; i <= stringToPrint.length(); i++){
				this.print("─");
			}
			this.println("┐");

			//middle line of box
			this.println("│" + stringToPrint + "│");

			//bottom line of box; index 1 to allow the different first character
			this.print("└");
			for(int i = 1; i <= stringToPrint.length(); i++){
				this.print("─");
			}
			this.println("┘");
		}
	}

	/**
	 * UI is a utility class that provides convenient methods for
	 * printing and obtaining text from the console.
	 */
	public UI(Scanner scanner)
	{
		this.scanner = scanner;
	}
}
