package com.robertcboyd.TextBasedGame.Base;

import java.util.ArrayList;
import java.util.List;

import com.robertcboyd.TextBasedGame.GameObjectInterfaces.*;

import com.robertcboyd.TextBasedGame.Parser.Word;

/**
 * The class representing the player. Provides many methods
 * essential to interacting with the player.
 */
public class Player extends GameObject implements Movable {
	private int hp;
	private ArrayList<Item> inventory;

	public ArrayList<Interactive> getEverythingInSight()
	{
		ArrayList<GameObject> everythingNearby = getLocation().getEverything();

		ArrayList<Interactive> everythingVisible = new ArrayList<Interactive>();
		for(GameObject thing:everythingNearby){
			if(thing.isVisibleTo(this)){
				everythingVisible.add(thing);
			}
		}

		//add the current location
		everythingVisible.add(this.getLocation());
		//add the player himself
		//FIXME is this necessary? It shouldn't be - Places should know if they
		//contain players
		everythingVisible.add(this);
		return everythingVisible;
	}

	@Override
	public void moveTo(Place p)
	{
		this.location = p;
	}

	public Place getPlaceNorthOf()
	{
		return this.location.getPlaceNorthOf();
	}

	public Place getPlaceEastOf()
	{
		return this.location.getPlaceEastOf();
	}

	public Place getPlaceSouthOf()
	{
		return this.location.getPlaceSouthOf();
	}

	public Place getPlaceWestOf()
	{
		return this.location.getPlaceWestOf();
	}

	public Player(UI ui, int hp, ArrayList<String> identifiers, ArrayList<Item> inventory, Place startingLocation)
	{
		super(ui, "", "It's you. What did you expect to see?", startingLocation, true, identifiers);
		this.hp = hp;
		this.inventory = inventory;
	}
}
