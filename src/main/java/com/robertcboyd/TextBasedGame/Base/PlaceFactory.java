package com.robertcboyd.TextBasedGame.Base;

import java.util.ArrayList;
import java.util.Arrays;

import com.robertcboyd.TextBasedGame.GameObjectInterfaces.GameObject;
import com.robertcboyd.TextBasedGame.Parser.WordParser;
import com.robertcboyd.TextBasedGame.Parser.Word;

public class PlaceFactory {
	private WordParser wp;
	private UI ui;
	private WorldManager wm;

	public Place createPlace(String description, GameObject... thingsHere)
	{
		return new Place("", description, wp, ui, wm, getIdentifiers(), new ArrayList<GameObject>(Arrays.asList(thingsHere)));
	}
	
	public Place createPlace(String name, String description, GameObject... thingsHere)
	{
		return new Place(name, description, wp, ui, wm, getIdentifiers(), new ArrayList<GameObject>(Arrays.asList(thingsHere)));
	}

	public Place createPlace(String name, String description, ArrayList<String> additionalIdentifiers, GameObject... thingsHere)
	{
		ArrayList<String> identifiers = getIdentifiers();

		for(String s:additionalIdentifiers){
			identifiers.add(s);
		}

		return new Place(name, description, wp, ui, wm, identifiers, new ArrayList<GameObject>(Arrays.asList(thingsHere)));
	}

	private ArrayList<String> getIdentifiers()
	{
		ArrayList<String> identifiers = new ArrayList<String>();
		identifiers.add("here");
		identifiers.add("around");
		return identifiers;
	}

	public PlaceFactory(WordParser wp, UI ui, WorldManager wm)
	{
		this.wp = wp;
		this.ui = ui;
		this.wm = wm;
	}
}
