package com.robertcboyd.TextBasedGame.Base;

import com.robertcboyd.TextBasedGame.Base.UI;

import java.util.ArrayList;

public class PlayerFactory {
	private UI ui;
	public Player createPlayer(Place startingLocation)
	{
		ArrayList<String> playerIdentifers = new ArrayList<String>(3);
		playerIdentifers.add("me");
		playerIdentifers.add("myself");
		playerIdentifers.add("I");

		ArrayList<Item> empty = new ArrayList<Item>();
		return new Player(ui, 100, playerIdentifers, empty, startingLocation);
	}

	public PlayerFactory(UI ui)
	{
		this.ui = ui;
	}
}
