package com.robertcboyd.TextBasedGame.Base;

import java.util.ArrayList;

import com.robertcboyd.TextBasedGame.Base.Direction;

/**
 * Helper class that understands how to attach and manage a series of connected Places.
 * The current backend for the roomInstance.getLocation() and roomInstance.attachTo*Of()
 * methods.
 */
public class WorldManager {

	/**
	 * A ArrayList containing all of the exit objects ever created.
	 * Used internally for looking up which exits exist.
	 */
	private ArrayList<Exit> exits = new ArrayList<Exit>();

	/**
	 * Creates 2 Exits: one from place1 to place2 heading in direction,
	 * and the other from place2 to place1 heading in the direction's opposite.
	 * This is usually what you want. The attachTo*Of methods are just wrappers
	 * around this method.
	 *
	 * @param place1 Place to be attached to the north, south, east, or west of place2
	 * @param direction the desired location of place1 respective to place2
	 * @param place2 base Place that place1 is being attached to
	 */
	private void attach(Place place1, Direction direction, Place place2)
	{
		Exit exitToPlace2 = new Exit(place2, place1, direction);
		exits.add(exitToPlace2);
		Exit exitToPlace1 = new Exit(place1, place2, direction.getOpposite());
		exits.add(exitToPlace1);
	}

	/**
	 * Creates 2 Exits: one from place1 to place2 heading north,
	 * and the other from place2 to place1 heading south.
	 * This is usually what you want.
	 *
	 * @param place1 Place to be attached to the north of place2
	 * @param place2 base Place that place1 is being attached to
	 */
	public void attachToNorthOf(Place place1, Place place2)
	{
		attach(place1, Direction.NORTH, place2);
	}

	/**
	 * Creates 2 Exits: one from place1 to place2 heading east,
	 * and the other from place2 to place1 heading west.
	 * This is usually what you want.
	 *
	 * @param place1 Place to be attached to the east of place2
	 * @param place2 base Place that place1 is being attached to
	 */
	public void attachToEastOf(Place place1, Place place2)
	{
		attach(place1, Direction.EAST, place2);
	}

	/**
	 * Creates 2 Exits: one from place1 to place2 heading south,
	 * and the other from place2 to place1 heading north.
	 * This is usually what you want.
	 *
	 * @param place1 Place to be attached to the south of place2
	 * @param place2 base Place that place1 is being attached to
	 */
	public void attachToSouthOf(Place place1, Place place2)
	{
		attach(place1, Direction.SOUTH, place2);
	}

	/**
	 * Creates 2 Exits: one from place1 to place2 heading west,
	 * and the other from place2 to place1 heading east.
	 * This is usually what you want.
	 *
	 * @param place1 Place to be attached to the west of place2
	 * @param place2 base Place that place1 is being attached to
	 */
	public void attachToWestOf(Place place1, Place place2)
	{
		attach(place1, Direction.WEST, place2);
	}

	/**
	 * Uses the internal ArrayList<Exit> to get the Place in
	 * a given Direction in relation to the passed in Place.
	 *
	 * @param direction the direction from the passed in Place to look
	 * @param place the place from which to look in the given Direction
	 *
	 * @return the place in direction from place, or null if none exists
	 */
	public Place getPlace(Direction direction, Place place)
	{
		for(Exit e:exits){
			if(e.getDirection() == direction && e.getOrigin().equals(place)){
				return e.getDestination();
			}
		}
		return null;
	}

	/**
	 * Returns the place to the north of the given Place.
	 * Wraps getPlace().
	 *
	 * @param place the location being queried
	 *
	 * @return the Place north of the given place, or null if none exists
	 */
	public Place getPlaceNorthOf(Place place)
	{
		return getPlace(Direction.NORTH, place);
	}

	/**
	 * Returns the place to the east of the given Place.
	 * Wraps getPlace().
	 *
	 * @param place the location being queried
	 *
	 * @return the Place east of the given place, or null if none exists
	 */
	public Place getPlaceEastOf(Place place)
	{
		return getPlace(Direction.EAST, place);
	}

	/**
	 * Returns the place to the south of the given Place.
	 * Wraps getPlace().
	 *
	 * @param place the location being queried
	 *
	 * @return the Place south of the given place, or null if none exists
	 */
	public Place getPlaceSouthOf(Place place)
	{
		return getPlace(Direction.SOUTH, place);
	}

	/**
	 * Returns the place to the west of the given Place.
	 * Wraps getPlace().
	 *
	 * @param place the location being queried
	 *
	 * @return the Place west of the given place, or null if none exists
	 */
	public Place getPlaceWestOf(Place place)
	{
		return getPlace(Direction.WEST, place);
	}

	/**
	 * Retrieves the Exit in a given Direction from a given
	 * Place if one exists.
	 *
	 * @param direction the Direction in which to look
	 * @param place the Place from which to look
	 *
	 * @return the Exit in the given Direction from the given
	 * Place, or null if none exists
	 */
	public Exit getExit(Direction direction, Place place)
	{
		//null checks for optimization purposes
		if(direction != null && place != null){
			for(Exit e:exits){
				if(e.getDirection() == direction && e.getOrigin().equals(place)){
					return e;
				}
			}
		}
		return null;
	}

	/**
	 * Retrieves the Exit to the north of the given Place
	 * if it exists. Wraps getExit().
	 *
	 * @param place the Place from which to look
	 *
	 * @return the Exit to the north of the given
	 * Place, or null if none exists
	 */
	public Exit getExitNorth(Place place)
	{
		return getExit(Direction.NORTH, place);
	}

	/**
	 * Retrieves the Exit to the east of the given Place
	 * if it exists. Wraps getExit().
	 *
	 * @param place the Place from which to look
	 *
	 * @return the Exit to the east of the given
	 * Place, or null if none exists
	 */
	public Exit getExitEast(Place place)
	{
		return getExit(Direction.EAST, place);
	}

	/**
	 * Retrieves the Exit to the south of the given Place
	 * if it exists. Wraps getExit().
	 *
	 * @param place the Place from which to look
	 *
	 * @return the Exit to the south of the given
	 * Place, or null if none exists
	 */
	public Exit getExitSouth(Place place)
	{
		return getExit(Direction.SOUTH, place);
	}

	/**
	 * Retrieves the Exit to the west of the given Place
	 * if it exists. Wraps getExit().
	 *
	 * @param place the Place from which to look
	 *
	 * @return the Exit to the west of the given
	 * Place, or null if none exists
	 */
	public Exit getExitWest(Place place)
	{
		return getExit(Direction.WEST, place);
	}

	/**
	 * This class defines the Exit object, which is used to connect all Places
	 * under this implementation of WorldManager.
	 */
	private class Exit {

		/** The starting location of the Exit. */
		private Place origin;

		/** The ending point of the Exit. */
		private Place destination;

		/** The direction in which the Exit leads. */
		private Direction direction;

		/**
		 * Called when the player attempts to use this exit.
		 * If you want to implement some kind of special check
		 * or effect upon exiting a particular way, override this
		 * method in a custom Exit.
		 *
		 * @param player the exiting player
		 */
		public void onExit(Player player)
		{
			player.moveTo(destination);
		}

		/**
		 * Returns the originating location for this Exit.
		 * This is where a player is leaving by taking this exit.
		 *
		 * @return the originating location of this Exit
		 */
		public Place getOrigin()
		{
			return origin;
		}

		/**
		 * Returns the destination location for this Exit.
		 * The destination location is the Place where the
		 * player will end up after taking this exit.
		 *
		 * @return the destination of this Exit
		 */
		public Place getDestination()
		{
			return destination;
		}

		/**
		 * Returns the (cardinal) direction in which this Exit
		 * goes.
		 *
		 * @return the Direction the Exit travels in
		 */
		public Direction getDirection()
		{
			return direction;
		}

		/**
		 * Constructs Exit objects
		 *
		 * @param origin the starting location for the Exit
		 * @param destination the final location for the Exit
		 * @param direction the Direction in which the Exit travels
		 *
		 * @return an Exit from origin to destination, in the given direction
		 */
		public Exit(Place origin, Place destination, Direction direction)
		{
			this.origin = origin;
			this.destination = destination;
			this.direction = direction;
		}
	}
}
