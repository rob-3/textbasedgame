package com.robertcboyd.TextBasedGame.Base;

import java.util.ArrayList;

import com.robertcboyd.TextBasedGame.GameObjectInterfaces.*;

import com.robertcboyd.TextBasedGame.Parser.WordParser;
import com.robertcboyd.TextBasedGame.Parser.Word;

public class Place implements Interactive, Describable, Lookable, Burnable {
	private WordParser wp;
	private ArrayList<GameObject> thingsHere;
	private WorldManager worldManager;
	private UI ui;
	private String description;
	private String name;
	private ArrayList<String> identifiers;

	@Override
	public void onBurn(){
		ui.println("What kind of pyromaniac are you?");
	}

	@Override
	public void onLook(){
		this.printDescription();
	}

	//TODO docs
	@Override
	public ArrayList<String> getIdentifiers(){
		return this.identifiers;
	}

	//TODO docs
	//This is a Place internal method only now
	public void onGo(Player player){
		player.moveTo(this);
	}

	//TODO docs
	@Override
	public void invoke(Command command, Player player)
	{
		switch(command){
			case look:
				this.onLook();
				break;
			case burn:
				this.onBurn();
				break;
			case go:
				this.onGo(player);
				break;
			default:
				break;
		}
	}


	/**
	 * Called when a Player enters the Place.
	 * Prints out the Place's description and loops
	 * while the Player remains in the Place.
	 *
	 * @param player entering Player
	 */
	public void onEnter(Player player)
	{
		this.printDescription();
		do {
			wp.processInput(ui.getInput(), player);
		} while(player.isHere(this));
	}

	/**
	 * Prints the description of this Place to the console.
	 */
	@Override
	public void printDescription()
	{
		ui.printInBox(this.name, true);
		ui.println(this.description);
	}

	public boolean containsThing(GameObject go)
	{
		if(go.getLocation().equals(this)){
			return true;
		} else {
			return false;
		}
	}

	//TODO update docs
	/**
	 * Standard Place constructor.
	 *
	 * @param name Desired name for the new Place
	 * @param description Desired description for the new Place
	 * @param wp the WordParser to use for processing
	 * @param ui the ui to use for text operations
	 *
	 * @return The newly constructed Place
	 */
	public Place(String name, String description, WordParser wp, UI ui, WorldManager wm, ArrayList<String> identifiers, ArrayList<GameObject> thingsHere)
	{
		this.ui = ui;
		this.name = name;
		this.description = description;
		this.identifiers = identifiers;
		this.wp = wp;
		this.worldManager = wm;
		this.thingsHere = thingsHere;
	}

	/**
	 * Gets the Place to the north of this Place.
	 *
	 * @return Place to the north if one exists, otherwise null.
	 */
	public Place getPlaceNorthOf()
	{
		return worldManager.getPlaceNorthOf(this);
	}

	/**
	 * Gets the Place to the east of this Place.
	 *
	 * @return Place to the east if one exists, otherwise null.
	 */
	public Place getPlaceEastOf()
	{
		return worldManager.getPlaceEastOf(this);
	}

	/**
	 * Gets the Place to the south of this Place.
	 *
	 * @return Place to the south if one exists, otherwise null.
	 */
	public Place getPlaceSouthOf()
	{
		return worldManager.getPlaceSouthOf(this);
	}

	/**
	 * Gets the Place to the west of this Place.
	 *
	 * @return Place to the west if one exists, otherwise null.
	 */
	public Place getPlaceWestOf()
	{
		return worldManager.getPlaceWestOf(this);
	}

	/**
	 * Attaches a Place to the north of this one.
	 *
	 * @param place the Place to be attached to the north
	 */
	public void attachToNorthOf(Place place)
	{
		worldManager.attachToNorthOf(this, place);
	}

	/**
	 * Attaches a Place to the east of this one.
	 *
	 * @param place the Place to be attached to the east
	 */
	public void attachToEastOf(Place place)
	{
		worldManager.attachToEastOf(this, place);
	}

	/**
	 * Attaches a Place to the south of this one.
	 *
	 * @param place the Place to be attached to the south
	 */
	public void attachToSouthOf(Place place)
	{
		worldManager.attachToSouthOf(this, place);
	}

	/**
	 * Attaches a Place to the west of this one.
	 *
	 * @param place the Place to be attached to the west
	 */
	public void attachToWestOf(Place place)
	{
		worldManager.attachToWestOf(this, place);
	}

	public ArrayList<GameObject> getEverything()
	{
		return this.thingsHere;
	}
}
