package com.robertcboyd.TextBasedGame.Base;

import com.robertcboyd.TextBasedGame.Base.Direction;

/**
 * This class defines and Exit object, which is used to connect all Places.
 * It's use is mostly automatic unless one wants custom actions upon exit
 * or entrance of a Place.
 */
public class Exit {

	/** The starting location of the Exit. */
	private Place origin;

	/** The ending point of the Exit. */
	private Place destination;

	/** The direction in which the Exit leads. */
	private Direction direction;

	/**
	 * Called when the player attempts to use this exit.
	 * If you want to implement some kind of special check
	 * or effect upon exiting a particular way, override this
	 * method in a custom Exit.
	 *
	 * @param player the exiting player
	 */
	public void onExit(Player player)
	{
		player.moveTo(destination);
	}

	/**
	 * Returns the originating location for this Exit.
	 * This is where a player is leaving by taking this exit.
	 *
	 * @return the originating location of this Exit
	 */
	public Place getOrigin()
	{
		return origin;
	}

	/**
	 * Returns the destination location for this Exit.
	 * The destination location is the Place where the
	 * player will end up after taking this exit.
	 *
	 * @return the destination of this Exit
	 */
	public Place getDestination()
	{
		return destination;
	}

	/**
	 * Returns the (cardinal) direction in which this Exit
	 * goes.
	 *
	 * @return the Direction the Exit travels in
	 */
	public Direction getDirection()
	{
		return direction;
	}

	/**
	 * Constructs Exit objects
	 *
	 * @param origin the starting location for the Exit
	 * @param destination the final location for the Exit
	 * @param direction the Direction in which the Exit travels
	 *
	 * @return an Exit from origin to destination, in the given direction
	 */
	public Exit(Place origin, Place destination, Direction direction)
	{
		this.origin = origin;
		this.destination = destination;
		this.direction = direction;
	}
}
