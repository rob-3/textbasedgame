package com.robertcboyd.TextBasedGame.Parser;

/**
 * A Word is a String plus its part of speech.
 */
public class Word {

	/**
	 * The String representation of the Word
	 */
	private String text;

	/**
	 * The part of speech of the Word
	 */
	private PartOfSpeech pos;

	/**
	 * Currently supports verb, obj(ects), and prep(ositions)
	 */
	public enum PartOfSpeech{
		obj,
		verb,
		prep
	}

	/**
	 * Gives the part of speech for the Word determined at the time of the Word's initialization.
	 *
	 * @return The correct PartOfSpeech, or null on failure
	 */
	public PartOfSpeech getPartOfSpeech()
	{
		return pos;
	}

	/**
	 * Converts the Word into a simple String
	 *
	 * @return A String representing the Word
	 */
	public String toString()
	{
		return text;
	}

	/**
	 * Get the proper indefinite article for the Word
	 *
	 * @return "a" or "an", as appropriate
	 */
	public String getIndefiniteArticle()
	{
		char firstChar = this.toString().charAt(0);
		switch(firstChar){
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
				return "an";
			default:
				return "a";
		}
		//FIXME add section for words like honor
	}

	/**
	 * Constructs Words
	 *
	 * @param text The String representation of the Word to be built
	 * @param pos The PartOfSpeech of the Word to be built
	 *
	 * @return The newly constructed Word
	 */
	public Word (String text, PartOfSpeech pos)
	{
		this.text = text;
		this.pos = pos;
	}
}
