package com.robertcboyd.TextBasedGame.Parser;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import com.robertcboyd.TextBasedGame.Base.*;
import com.robertcboyd.TextBasedGame.GameObjectInterfaces.*;

import com.robertcboyd.TextBasedGame.Parser.Word.PartOfSpeech;

import static com.robertcboyd.TextBasedGame.Base.Command.*;

/**
 * A class to clean up, tokenize, and process user input.
 */
public class WordParser {

	//TODO store aliases in a HashMap
	//TODO allow for user provided aliases
	private Map<String, String> aliases = new HashMap<String, String>();

	/**
	 * Expands aliases in the input String.
	 *
	 * @param input the String to be expanded
	 */
	public String processAliases(String input)
	{
		switch(input){
			case "n":
			case "north":
				return "go north";
			case "e":
			case "east":
				return "go east";
			case "s":
			case "south":
				return "go south";
			case "w":
			case "west":
				return "go west";
			case "l":
			case "look":
				return "look around";
			default:
				return input;
		}
	}

	//TODO examine if the raw input ought to be stored somewhere in case of failures to parse
	/**
	 * Cleans up the raw user input and removes nonletter, nonnumber characters
	 *
	 * @param rawInput String containing the raw user input
	 *
	 * @return The revised, cleaned up input
	 */
	public String parse(String rawInput)
	{
		return rawInput.toLowerCase().trim().replaceAll(" +", " ").replaceAll("[^a-zA-Z0-9\\s]", "");
	}

	/**
	 * Utility method that tokenizes (splits) the user's input into
	 * individual Strings. This method also finds the part of speech for
	 * each String and stores the results of these calcuations in a
	 * ArrayList<Word>.
	 *
	 * @param cleanInput Sanitized user input without multiple spaces.
	 *
	 * @return A ArrayList<Word> containing the tokenized Strings converted to Words (data objects containing a String and a PartOfSpeech)
	 */
	public ArrayList<Word> tokenize(String cleanInput)
	{
		//split based on whitespace
		String[] stringList = cleanInput.split("\\s+");

		//declare new ArrayList with words
		ArrayList<Word> wordList = new ArrayList<Word>();
		for (String s:stringList) {
			wordList.add(new Word(s, getPartOfSpeech(stringList, s)));
		}

		return wordList;
	}

	/**
	 * Attempts to discern the PartOfSpeech for a word (using context) as best as it can. This method always gives its best guess and will throw an exception if null or empty input is given.
	 * NOTE: the above is not true at this time
	 *
	 * @param tokenizedCommand A nonnull String array containing the cleaned up, tokenized user input
	 * @param wordToCheck The String containing the word in question to determine the PartOfSpeech for
	 *
	 * @return The program's best guess at the PartOfSpeech for a String
	 */
	public PartOfSpeech getPartOfSpeech(String[] tokenizedCommand, String wordToCheck)
	{
		//TODO fix this
		return PartOfSpeech.verb;
	}

	/**
	 * This method takes the user's raw input as a String and runs the appropriate Command. It does null checks to assure that the input is valid.
	 *
	 * @param rawInput The unmodified user input read from the console
	 */
	public void processInput(String rawInput, Player player)
	{
		Sentence sentence = new Sentence(this.tokenize(this.processAliases(this.parse(rawInput))));
		//TODO ensure that the Sentence is an actual good object

		//bind sentence to a Command
		Command c = getCommandFromSentence(sentence);
		//TODO null check

		//invoke a Command on a GameObject if the verb has an object
		//otherwise execute with executeIntransitiveVerb()
		if(isVerbTransitive(sentence)){
			Interactive object = resolveIdentifier(sentence.getObject(), player);
			//TODO null check on object
			if(object != null){
				object.invoke(c, player);
			} else {
				Game.getLogger().log("Couldn't identify object from String '" + sentence.getObject() + "'.");
			}
			
		} else {
			executeIntransitiveVerb(c);
		}

	}

	private void executeIntransitiveVerb(Command intransitiveVerb)
	{
		//switch
		//TODO implementation
	}

	private Command getCommandFromSentence(Sentence sentence)
	{
		switch(sentence.getVerb()){
			case "go":
				return go;
			case "look":
				return look;
			default:
				return null;
		}
		//TODO implement better
	}

	//TODO docs
	private Interactive resolveIdentifier(String identifierToResolve, Player player)
	{
		//try to resolve the identifier to a place
		Place potentialPlace;
		switch(identifierToResolve){
			case "north":
				potentialPlace = player.getPlaceNorthOf();
				break;
			case "south":
				potentialPlace = player.getPlaceSouthOf();
				break;
			case "east":
				potentialPlace = player.getPlaceEastOf();
				break;
			case "west":
				potentialPlace = player.getPlaceWestOf();
				break;
			default:
				potentialPlace = null;
				break;
		}
		if(potentialPlace != null){
			return potentialPlace;
		}

		//failing one of those special cases, start checking the identifier
		//against random stuff lying around

		ArrayList<Interactive> everythingNearby = player.getEverythingInSight();

		for(Interactive visibleThing:everythingNearby){
			ArrayList<String> visibleThingIdentifiers = visibleThing.getIdentifiers();
			for(String identifier:visibleThingIdentifiers){
				if(identifier.equals(identifierToResolve)){
					//TODO support for muliple matching objects
					return visibleThing;
				}
			}
		}
		//failing everything, return null
		//FIXME probably a bad idea to return null
		return null;
	}

	private boolean isVerbTransitive(Sentence sentence)
	{
		if(sentence.hasObject()){
			return true;
		} else {
			return false;
		}
	}
}
