package com.robertcboyd.TextBasedGame.Parser;

import java.util.ArrayList;

/**
 * Represents a Sentence as a ArrayList<Word> and
 * provides useful methods such as getVerb() and
 * getObject().
 */
public class Sentence {
	/**
	 * A tokenized (split into words) ArrayList of Words
	 */
	private ArrayList<Word> tokenizedRealInput;

	/**
	 * Gets the verb for the Sentence. Currently simply selects the first word, as the first word is frequently the verb.
	 *
	 * @return The verb in the Sentence, null if none or unsure.
	 */
	public String getVerb()
	{
		if (tokenizedRealInput != null) {
			return tokenizedRealInput.get(0).toString();
		} else {
			return null;
		}
	}

	/**
	 * Gets the object for the Sentence.
	 *
	 * @return The object in the Sentence, null if no object exists
	 */
	public String getObject()
	{
		if (tokenizedRealInput != null) {
			return tokenizedRealInput.get(1).toString();
		} else {
			return null;
		}
		//TODO: This implementation is very bare bones and relies on the most common case
	}

	public boolean hasObject()
	{
		if(this.getObject() != null){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Creates an array of all of the object's modifiers.
	 * NOTE: untested
	 *
	 * @return The object in the Sentence, null if no such modifiers exist
	 */
	public Word[] getObjectModifiers()
	{
		if (tokenizedRealInput != null) {
			Word[] returnArray = new Word[tokenizedRealInput.size() - 2];
			for(int i = 2; i < tokenizedRealInput.size(); i++) {
				returnArray[i - 2] = tokenizedRealInput.get(i);
			}
			return returnArray;
		} else {
			return null;
		}
		//TODO: strengthen implementation
	}

	/**
	 * Constructs a sentence.
	 *
	 * @param tokenizedInput A ArrayList containing the tokenized words in the user's input, in order
	 *
	 * @return A newly constructed sentence
	 */
	public Sentence(ArrayList<Word> tokenizedInput)
	{
		this.tokenizedRealInput = tokenizedInput;
	}

}
