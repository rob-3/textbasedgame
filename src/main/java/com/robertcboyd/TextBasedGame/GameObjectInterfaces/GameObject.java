package com.robertcboyd.TextBasedGame.GameObjectInterfaces;

import java.util.ArrayList;

import com.robertcboyd.TextBasedGame.Base.Command;
import com.robertcboyd.TextBasedGame.Base.Place;
import com.robertcboyd.TextBasedGame.Base.Player;
import com.robertcboyd.TextBasedGame.Base.UI;
import com.robertcboyd.TextBasedGame.Parser.Word;

public abstract class GameObject implements Interactive, Lookable, Burnable, Describable, Movable {
	private String name;
	private String description;
	private ArrayList<String> identifiers;
	private boolean visible;

	protected UI ui;
	protected Place location;

	@Override
	public void moveTo(Place p){
		this.location = p;
	}

	public boolean isVisibleTo(GameObject go)
	{
		if(this.visible && go.isHere(this.getLocation())){
			return true;
		} else {
			return false;
		}
	}

	public boolean isHere(Place p)
	{
		if(this.location.equals(p)){
			return true;
		} else {
			return false;
		}
	}

	@Override
	public ArrayList<String> getIdentifiers()
	{
		return this.identifiers;
	}

	@Override
	public void invoke(Command command, Player player)
	{
		switch(command){
			case look:
				this.onLook();
				break;
			case burn:
				this.onBurn();
				break;
			default:
				break;
		}
	}

	@Override
	public void onLook()
	{
		this.printDescription();
	}

	@Override
	public void printDescription()
	{
		ui.println(this.getDescription().toString());
	}

	@Override
	public void onBurn()
	{
		ui.print("A " + this.getName().toString().toLowerCase() + " is not something that you should burn.");
	}

	//FIXME do I really need these random getters/setters?
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Place getLocation() {
		return location;
	}
	
	public GameObject(UI ui, String name, String description, Place location, boolean visible, ArrayList<String> identifiers)
	{
		this.name = name;
		this.description = description;
		this.ui = ui;
		this.location = location;
		this.visible = visible;
		this.identifiers = identifiers;
	}
}
