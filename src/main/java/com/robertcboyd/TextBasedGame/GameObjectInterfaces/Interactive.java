package com.robertcboyd.TextBasedGame.GameObjectInterfaces;

import java.util.ArrayList;

import com.robertcboyd.TextBasedGame.Base.Command;
import com.robertcboyd.TextBasedGame.Base.Player;

public interface Interactive {
	//Commands can be run with regard to this object
	public void invoke(Command c, Player p);
	//This object can be identified
	public ArrayList<String> getIdentifiers(); 
}
