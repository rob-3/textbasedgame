package com.robertcboyd.TextBasedGame.GameObjectInterfaces;

public interface Burnable {
	public void onBurn();
}
