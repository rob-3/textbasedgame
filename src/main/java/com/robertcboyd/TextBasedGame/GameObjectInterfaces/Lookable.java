package com.robertcboyd.TextBasedGame.GameObjectInterfaces;

public interface Lookable {
	public void onLook();
}
