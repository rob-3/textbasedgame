package com.robertcboyd.TextBasedGame.GameObjectInterfaces;

public interface Describable {
	public void printDescription();
}
