package com.robertcboyd.TextBasedGame.GameObjectInterfaces;

import com.robertcboyd.TextBasedGame.Base.Place;

public interface Movable {
	public void moveTo(Place p);
}
